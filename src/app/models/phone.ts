export interface Phone {
    id: number;
    studentId: number;
    phoneNumber: string;
    phoneType: string;
    countryCode: string;
    areaCode: string;
    createdOn: Date;
    updatedOn: Date;
}
