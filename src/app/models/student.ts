import { Address } from './address';
import { Email } from './email';
import { Phone } from './phone';

export interface Student {
    id: number;
    lastName: string;
    middleName?: string;
    firstName: string;
    gender: string;
    addresses?: Address[];
    emails?: Email[];
    phones?: Phone[];
}
