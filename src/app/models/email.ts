export interface Email {
    id: number;
    studentId: number;
    emailString: string;
    emailType: string;
    createdOn: Date;
    updatedOn: Date;
}
