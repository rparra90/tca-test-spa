export interface Address {
    id: number;
    studentId: number;
    addressLine: string;
    city: string;
    zipPostcode: string;
    state: string;
}
