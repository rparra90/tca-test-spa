import { Injectable } from '@angular/core';
import { Observable } from 'rxjs-compat';

@Injectable({
  providedIn: 'root'
})
export class ErrorService {

  constructor() { }

  handleError(error: any) {
    const errors = error.error;
    let modelStateErrors = '';
    for (const key in errors) {
      if (errors[key]) {
        modelStateErrors += JSON.stringify(errors[key]);
      }
    }
    return Observable.throw(modelStateErrors);
  }
}
