import { Injectable } from '@angular/core';
declare let alertify: any;

@Injectable({
  providedIn: 'root'
})
export class AlertService {

  constructor() { }

  confirm(message: string, okCallback: () => any) {
    alertify.confirm(message, function (e) {
      if (e) {
        okCallback();
      } else {

      }
    });
  }

  success(message: string) {
    console.log('Success: ' + message);
    alertify.success(message);
  }

  error(message: string) {
    console.log('Error: ' + message);
    alertify.error(message);
  }

  warning(message: string) {
    console.log('Warning: ' + message);
    alertify.warning(message);
  }

  message(message: string) {
    console.log(message);
    alertify.message(message);
  }
}
