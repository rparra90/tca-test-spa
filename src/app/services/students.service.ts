import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { ErrorService } from './error.service';
import { Student } from '../models/student';

import 'rxjs-compat/add/operator/map';
import 'rxjs-compat/add/operator/catch';

@Injectable({
  providedIn: 'root'
})
export class StudentsService {
  baseUrl = environment.apiUrl;

  constructor(
    private http: HttpClient,
    private errorService: ErrorService
  ) { }

  getStudent(id: number): Observable<Student> {
    return this.http
      .get(this.baseUrl + 'students/' + id)
      .map(response => <Student> response)
      .catch(this.errorService.handleError);
  }

  getStudents(): Observable<Student[]> {
    return this.http
      .get(this.baseUrl + 'students')
      .map(response => <Student[]> response)
      .catch(this.errorService.handleError);
  }

  postStudent(student: Student): Observable<any> {
    return this.http
      .post(this.baseUrl + 'students', student)
      .map(response => response)
      .catch(this.errorService.handleError);
  }

  putStudent(id: number, student: Student): Observable<any> {
    return this.http
      .put(this.baseUrl + 'students/' + id, student)
      .map(response => response)
      .catch(this.errorService.handleError);
  }

  deleteStudent(id: number): Observable<any> {
    return this.http
      .delete(this.baseUrl + 'students/' + id)
      .map(response => response)
      .catch(this.errorService.handleError);
  }

}
