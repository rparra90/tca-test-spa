import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ErrorService } from './error.service';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Phone } from '../models/phone';

@Injectable({
  providedIn: 'root'
})
export class PhoneService {
  baseUrl = environment.apiUrl;

  constructor(
    private http: HttpClient,
    private errorService: ErrorService
  ) { }

  getPhone(id: number): Observable<Phone> {
    return this.http
      .get(this.baseUrl + 'phones/' + id)
      .map(response => <Phone> response)
      .catch(this.errorService.handleError);
  }

  getPhones(): Observable<Phone[]> {
    return this.http
      .get(this.baseUrl + 'phones')
      .map(response => <Phone[]> response)
      .catch(this.errorService.handleError);
  }

  postPhone(phone: Phone): Observable<any> {
    return this.http
      .post(this.baseUrl + 'phones', phone)
      .map(response => response)
      .catch(this.errorService.handleError);
  }

  putPhone(id: number, phone: Phone): Observable<any> {
    return this.http
      .put(this.baseUrl + 'phones/' + id, phone)
      .map(response => response)
      .catch(this.errorService.handleError);
  }

  deletePhone(id: number): Observable<any> {
    return this.http
      .delete(this.baseUrl + 'phones/' + id)
      .map(response => response)
      .catch(this.errorService.handleError);
  }

}
