import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ErrorService } from './error.service';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Address } from '../models/address';

@Injectable({
  providedIn: 'root'
})
export class AddressService {
  baseUrl = environment.apiUrl;

  constructor(
    private http: HttpClient,
    private errorService: ErrorService
  ) { }

  getAddress(id: number): Observable<Address> {
    return this.http
      .get(this.baseUrl + 'addresses/' + id)
      .map(response => <Address> response)
      .catch(this.errorService.handleError);
  }

  getAddresses(): Observable<Address[]> {
    return this.http
      .get(this.baseUrl + 'addresses')
      .map(response => <Address[]> response)
      .catch(this.errorService.handleError);
  }

  postAddress(address: Address): Observable<any> {
    return this.http
      .post(this.baseUrl + 'addresses', address)
      .map(response => response)
      .catch(this.errorService.handleError);
  }

  putAddress(id: number, address: Address): Observable<any> {
    return this.http
      .put(this.baseUrl + 'addresses/' + id, address)
      .map(response => response)
      .catch(this.errorService.handleError);
  }

  deleteAddress(id: number): Observable<any> {
    return this.http
      .delete(this.baseUrl + 'addresses/' + id)
      .map(response => response)
      .catch(this.errorService.handleError);
  }

}
