import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ErrorService } from './error.service';
import { Email } from '../models/email';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class EmailService {
  baseUrl = environment.apiUrl;

  constructor(
    private http: HttpClient,
    private errorService: ErrorService
  ) { }

  getEmail(id: number): Observable<Email> {
    return this.http
      .get(this.baseUrl + 'emails/' + id)
      .map(response => <Email> response)
      .catch(this.errorService.handleError);
  }

  getEmails(): Observable<Email[]> {
    return this.http
      .get(this.baseUrl + 'emails')
      .map(response => <Email[]> response)
      .catch(this.errorService.handleError);
  }

  postEmail(email: Email): Observable<any> {
    return this.http
      .post(this.baseUrl + 'emails', email)
      .map(response => response)
      .catch(this.errorService.handleError);
  }

  putEmail(id: number, email: Email): Observable<any> {
    return this.http
      .put(this.baseUrl + 'emails/' + id, email)
      .map(response => response)
      .catch(this.errorService.handleError);
  }

  deleteEmail(id: number): Observable<any> {
    return this.http
      .delete(this.baseUrl + 'emails/' + id)
      .map(response => response)
      .catch(this.errorService.handleError);
  }

}
