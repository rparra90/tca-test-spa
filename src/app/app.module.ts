import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { StudentsListComponent } from './components/students-list/students-list.component';
import { StudentsService } from './services/students.service';
import { ErrorService } from './services/error.service';
import { HttpClientModule } from '@angular/common/http';
import { NavbarComponent } from './components/navbar/navbar.component';
import { AppRoutingModule } from './app-routing.module';
import { StudentDetailComponent } from './components/student-detail/student-detail.component';
import { EmailService } from './services/email.service';
import { PhoneService } from './services/phone.service';
import { AddressService } from './services/address.service';
import { AlertService } from './services/alert.service';

@NgModule({
  declarations: [
    AppComponent,
    StudentsListComponent,
    NavbarComponent,
    StudentDetailComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule
  ],
  providers: [
    ErrorService,
    StudentsService,
    EmailService,
    PhoneService,
    AddressService,
    AlertService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
