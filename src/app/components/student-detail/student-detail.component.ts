import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { StudentsService } from 'src/app/services/students.service';
import { ActivatedRoute } from '@angular/router';
import { Student } from 'src/app/models/student';
import { EmailService } from 'src/app/services/email.service';
import { AddressService } from 'src/app/services/address.service';
import { PhoneService } from 'src/app/services/phone.service';
import { Email } from 'src/app/models/email';
import { Address } from 'src/app/models/address';
import { Phone } from 'src/app/models/phone';
import { AlertService } from 'src/app/services/alert.service';

@Component({
  selector: 'app-student-detail',
  templateUrl: './student-detail.component.html',
  styleUrls: ['./student-detail.component.sass']
})
export class StudentDetailComponent implements OnInit {
  @ViewChild('closeAddEmailModal') closeAddEmailModal: ElementRef;
  @ViewChild('closeAddPhoneModal') closeAddPhoneModal: ElementRef;
  @ViewChild('closeAddAddressModal') closeAddAddressModal: ElementRef;

  student: Student;

  studentFormModel: any = {};
  emailFormModel: any = {};
  addressFormModel: any = {};
  phoneFormModel: any = {};

  editMode = false;
  editModeModal = false;

  constructor(
    private route: ActivatedRoute,
    private studentService: StudentsService,
    private emailService: EmailService,
    private addressService: AddressService,
    private phoneService: PhoneService,
    private alertService: AlertService
  ) { }

  updateStudent(id: number) {
    this.studentService.getStudent(id).subscribe(response => {
      console.log(response);
      this.student = response;
      this.studentFormModel = response;
    }, error => {
      console.error(error);
      this.alertService.error(error);
    });
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      const userId = params['id'];
      this.updateStudent(userId);
    });
  }

  editStudent() {
    console.log(this.studentFormModel);
    this.studentService.putStudent(this.student.id, this.studentFormModel).subscribe(response => {
      console.log(response);
      this.editMode = false;
      this.alertService.success('Successfully edited');
    }, error => {
      console.error(error);
      this.alertService.error(error);
    });
  }

  activateEditMode() {
    this.editMode = true;
  }

  discardChanges() {
    this.updateStudent(this.student.id);
    this.editMode = false;
  }

  setEmailModel(email: Email) {
    console.log(email);
    this.emailFormModel = email;
    this.editModeModal = true;
  }

  setAddressModel(address: Address) {
    console.log(address);
    this.addressFormModel = address;
    this.editModeModal = true;
  }

  setPhoneModel(phone: Phone) {
    console.log(phone);
    this.phoneFormModel = phone;
    this.editModeModal = true;
  }

  dismissModalEdit() {
    this.editModeModal = false;

    this.emailFormModel = {};
    this.addressFormModel = {};
    this.phoneFormModel = {};

    this.updateStudent(this.student.id);
  }

  addEmail() {
    const email = this.emailFormModel;
    email['studentId'] = this.student.id;

    console.log(email);
    this.emailService.postEmail(email).subscribe(response => {
      console.log(response);
      this.alertService.success('Successfully added');
      this.updateStudent(this.student.id);
      this.closeAddEmailModal.nativeElement.click();
      this.emailFormModel = {};
    }, error => {
      console.error(error);
      this.alertService.error(error);
    });
  }

  editEmail() {
    console.log(this.emailFormModel);
    this.emailService.putEmail(this.emailFormModel.id, this.emailFormModel).subscribe(response => {
      console.log(response);
      this.updateStudent(this.student.id);
      this.alertService.success('Successfully edited');
      this.editModeModal = false;
      this.emailFormModel = {};
      this.closeAddEmailModal.nativeElement.click();
    }, error => {
      console.error(error);
      this.alertService.error(error);
    });
  }

  deleteEmail(id: number) {
    this.alertService.confirm('Are you sure you want to delete this email?', () => {
      this.emailService.deleteEmail(id).subscribe(response => {
        console.log(response);
        this.updateStudent(this.student.id);
      }, error => {
        console.error(error);
        this.alertService.error(error);
      });
    });
  }

  addAddress() {
    const address = this.addressFormModel;
    address['studentId'] = this.student.id;

    this.addressService.postAddress(address).subscribe(response => {
      console.log(response);
      this.alertService.success('Successfully added');
      this.updateStudent(this.student.id);
      this.closeAddAddressModal.nativeElement.click();
      this.addressFormModel = {};
    }, error => {
      console.error(error);
      this.alertService.error(error);
    });
  }

  editAddress() {
    console.log('editAddress', 'addressFormModel', this.addressFormModel);
    this.addressService.putAddress(this.addressFormModel.id, this.addressFormModel).subscribe(response => {
      console.log('editAddress', 'response', response);
      this.updateStudent(this.student.id);
      this.editModeModal = false;
      this.addressFormModel = {};
      this.closeAddAddressModal.nativeElement.click();
      this.alertService.success('Successfully edited');
    }, error => {
      console.error('editAddress', 'error', error);
      this.alertService.error(error);
    });
  }

  deleteAddress(id: number) {
    this.alertService.confirm('Are you sure you want to delete this address?', () => {
      this.addressService.deleteAddress(id).subscribe(response => {
        console.log(response);
        this.alertService.success('yyd');
        this.updateStudent(this.student.id);
      }, error => {
        console.error(error);
        this.alertService.error(error);
      });
    });
  }

  addPhone() {
    const phone = this.phoneFormModel;
    phone['studentId'] = this.student.id;

    this.phoneService.postPhone(phone).subscribe(response => {
      console.log(response);
      this.updateStudent(this.student.id);
      this.closeAddPhoneModal.nativeElement.click();
      this.phoneFormModel = {};
      this.alertService.success('Successfully added');
    }, error => {
      console.error(error);
      this.alertService.error(error);
    });
  }

  editPhone() {
    console.log('editPhone', 'phoneFormModel', this.phoneFormModel);
    this.phoneService.putPhone(this.phoneFormModel.id, this.phoneFormModel).subscribe(response => {
      console.log('editPhone', 'response', response);
      this.updateStudent(this.student.id);
      this.editModeModal = false;
      this.addressFormModel = {};
      this.closeAddPhoneModal.nativeElement.click();
      this.alertService.success('Successfully edited');
    }, error => {
      console.error('editPhone', 'error', error);
      this.alertService.error(error);
    });
  }

  deletePhone(id: number) {
    this.alertService.confirm('Are you really sure you want to delete this phone?', () => {
      this.phoneService.deletePhone(id).subscribe(response => {
        console.log('deletePhone', 'response', response);
        this.updateStudent(this.student.id);
        this.alertService.success('Successfully deleted');
      }, error => {
        console.error('deletePhone', 'error', error);
        this.alertService.error(error);
      });
    });
  }
}
