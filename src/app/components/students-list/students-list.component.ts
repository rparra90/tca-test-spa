import { Component, OnInit } from '@angular/core';
import { ViewChild, ElementRef} from '@angular/core';
import { StudentsService } from 'src/app/services/students.service';
import { Student } from 'src/app/models/student';
import { AlertService } from 'src/app/services/alert.service';

@Component({
  selector: 'app-students-list',
  templateUrl: './students-list.component.html',
  styleUrls: ['./students-list.component.sass']
})
export class StudentsListComponent implements OnInit {
  students: Student[];
  studentFormModel: any = {};
  @ViewChild('closeAddStudentModal') closeAddExpenseModal: ElementRef;

  constructor(
    private studentsService: StudentsService,
    private alertService: AlertService
  ) { }

  updateList() {
    this.studentsService.getStudents().subscribe(response => {
      this.students = response;
      console.log(response);
    }, error => {
      console.error(error);
      this.alertService.error(error);
    });
  }

  ngOnInit() {
    this.updateList();
  }

  addStudent() {
    console.log(this.studentFormModel);
    this.studentsService.postStudent(this.studentFormModel).subscribe(response => {
      console.log(response);
      this.updateList();
      this.closeAddExpenseModal.nativeElement.click();
      this.alertService.success('New student created');
    }, error => {
      console.error(error);
      this.alertService.error(error);
    });
  }

  deleteStudent(id: number) {
    this.alertService.confirm('Do you really want to delete this student?', () => {
      this.studentsService.deleteStudent(id).subscribe(response => {
        console.log(response);
        this.updateList();
      }, error => {
        console.error(error);
        this.alertService.error(error);
      });
    });
  }
}
